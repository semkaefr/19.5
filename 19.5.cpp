// 19.5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
using namespace std;

class Animal
{
private:
    string x = "I am an animal";
public:
    virtual void Voice() 
    {
        cout << x << endl;
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        cout << "Woof!" << endl;
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        cout << "Meow!" << endl;
    }
};

class Sheep : public Animal
{
public:
    void Voice() override
    {
        cout << "Meh!" << endl;
    }
};
int main()
{
    Animal* animals[3];
    Animal* d = new Dog();
    Animal* c = new Cat();
    Animal* sh = new Sheep();
    animals[0] = d;
    animals[1] = c;
    animals[2] = sh;
    for (int i = 0; i < 3; i++)
    {
        animals[i]->Voice();
    }
}
